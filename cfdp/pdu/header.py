from bitarray import bitarray
from bitarray.util import ba2int

from cfdp.meta import get_required_octets


CFDP_PROTOCOL_VERSION = 1


class PduHeader:

    def __init__(
            self,
            pdu_type=None,
            direction=None,
            transmission_mode=None,
            crc_flag=0,
            large_file_flag=0,
            pdu_data_field_length=None,
            segmentation_control=0,
            length_of_entity_ids=0,
            segmentation_metadata_flag=0,
            length_of_seq_number=0,
            source_entity_id=None,
            transaction_seq_number=None,
            destination_entity_id=None):

        if None in [
                pdu_type, direction, transmission_mode, source_entity_id,
                transaction_seq_number, destination_entity_id]:
            raise Exception("Must provide all required parameters")

        self.version = CFDP_PROTOCOL_VERSION
        self.pdu_type = pdu_type
        self.direction = direction
        self.transmission_mode = transmission_mode
        self.crc_flag = crc_flag
        self.large_file_flag = large_file_flag
        self.pdu_data_field_length = pdu_data_field_length
        self.segmentation_control = segmentation_control
        self.length_of_entity_ids = length_of_entity_ids
        self.segmentation_metadata_flag = segmentation_metadata_flag
        self.length_of_seq_number = length_of_seq_number
        self.source_entity_id = source_entity_id
        self.transaction_seq_number = transaction_seq_number
        self.destination_entity_id = destination_entity_id

    def to_bytes(self):
        if self.pdu_data_field_length is None:
            raise ValueError("PDU data field length not defined")

        self.length_of_entity_ids = max(
            get_required_octets(self.source_entity_id) - 1,
            get_required_octets(self.destination_entity_id) - 1)

        databits = bitarray(
            format(self.version, '03b') +
            format(self.pdu_type, '01b') +
            format(self.direction, '01b') +
            format(self.transmission_mode, '01b') +
            format(self.crc_flag, '01b') +
            format(self.large_file_flag, '01b') +
            format(self.pdu_data_field_length, '016b') +
            format(self.segmentation_control, '01b') +
            format(self.length_of_entity_ids, '03b') +
            format(self.segmentation_metadata_flag, '01b') +
            format(self.length_of_seq_number, '03b') +
            format(self.source_entity_id, '0' +
                   str(8 + self.length_of_entity_ids * 8) + 'b') +
            format(self.transaction_seq_number, '0' +
                   str(8 + self.length_of_seq_number * 8) + 'b') +
            format(self.destination_entity_id, '0' +
                   str(8 + self.length_of_entity_ids * 8) + 'b'))
        return databits.tobytes()

    def __len__(self):
        entity_id_field_length = 2 * (self.length_of_entity_ids + 1)
        transaction_seq_number_field_length = self.length_of_seq_number + 1
        return 4 + entity_id_field_length + transaction_seq_number_field_length

    @classmethod
    def from_pdu(cls, pdu):
        databits = bitarray(endian='big')
        databits.frombytes(pdu)

        pdu_data_field_length = ba2int(databits[8:24])
        length_of_entity_ids = ba2int(databits[25:28])
        length_of_seq_number = ba2int(databits[29:32])

        fixed_pdu_end = 32 + (2 * (8 + 8 * length_of_entity_ids))\
            + (8 + 8 * length_of_seq_number)
        source_entity_id = ba2int(databits[
            32:(32 + (8 + 8 * length_of_entity_ids))])
        transaction_seq_number = ba2int(databits[
            32 + (8 + 8 * length_of_entity_ids):
            32 + (8 + 8 * length_of_entity_ids)
            + (8 + 8 * length_of_seq_number)])
        destination_entity_id = ba2int(databits[
            32 + (8 + 8 * length_of_entity_ids)
            + (8 + 8 * length_of_seq_number):fixed_pdu_end])

        return cls(
            pdu_type=int(databits[3]),
            direction=int(databits[4]),
            transmission_mode=int(databits[5]),
            crc_flag=int(databits[6]),
            large_file_flag=int(databits[7]),
            pdu_data_field_length=pdu_data_field_length,
            segmentation_control=int(databits[23]),
            length_of_entity_ids=length_of_entity_ids,
            segmentation_metadata_flag=int(databits[26]),
            length_of_seq_number=length_of_seq_number,
            source_entity_id=source_entity_id,
            transaction_seq_number=transaction_seq_number,
            destination_entity_id=destination_entity_id)
