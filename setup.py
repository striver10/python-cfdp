from setuptools import setup, find_packages


setup(
    name='python-cfdp',
    version='0.0.1',
    description='Python implementation of CCSDS File Delivery Protocol',
    license='MIT',
    python_requires='>=3.4',
    keywords='ccsds cfdp file operation',
    packages=find_packages(exclude=['examples']),
    install_requires=[
        'bitarray'
    ],
    extras_require={
        'zmq': ['pyzmq']
    }
)
