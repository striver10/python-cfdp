# Python CFDP

Implementation of the CCSDS File Delivery Protocol in Python (see the docs folder). Currently it uses ZMQ as underlying transport layer. The goal is to
allow file transfer over radio link, using CCSDS frames as transport layer.

![Screenshot](docs/example.png)

## Getting Started

Clone the repository and then install via pip:

```bash
$ git clone https://gitlab.com/librecube/prototypes/python-cfdp
$ cd python-cfdp
$ pip install -e .
```

Start the remote entity first:

```bash
$ cd examples
$ python server.py
```

Then start one of the local entity examples:

```bash
$ cd examples
$ python example_send_file.py
```
