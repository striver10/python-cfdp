import logging

import cfdp
from cfdp.transport import ZmqTransport, UdpTransport
from cfdp.filestore import NativeFileStore

logging.basicConfig(level=logging.DEBUG)


#choice = int(input("Select transport (1=ZMQ, 2=UDP): "))
choice = 2
transport = ZmqTransport() if choice == 1 else UdpTransport()

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(1, "127.0.0.1:5551"),
    remote_entities=[cfdp.RemoteEntity(
        2, "127.0.0.1:5552", transaction_inactivity_limit=60)],
    filestore=NativeFileStore("./files/server"),
    transport=transport)

cfdp_entity = cfdp.CfdpEntity(config)
config.transport.bind()

input("Running. Press <Enter> to stop...\n")

config.transport.unbind()
cfdp_entity.shutdown()
