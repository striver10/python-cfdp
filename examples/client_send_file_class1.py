import logging
import time

import cfdp
from cfdp.transport import ZmqTransport, UdpTransport
from cfdp.filestore import NativeFileStore

logging.basicConfig(level=logging.DEBUG)


choice = int(input("Select transport (1=ZMQ, 2=UDP): "))
transport = ZmqTransport() if choice == 1 else UdpTransport()

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(1, "127.0.0.1:5551")],
    filestore=NativeFileStore("./files/client"),
    transport=ZmqTransport() if choice == 1 else UdpTransport())

cfdp_entity = cfdp.CfdpEntity(config)
config.transport.connect(remote_entity_id=1)

transaction_id = cfdp_entity.put(
    destination_id=1,
    source_filename="/small.txt",
    destination_filename="/small_remote.txt",
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED)

# print("Transaction frozen")
# cfdp_entity.freeze(transaction_id)
# input("Press enter to unfreeze")
# cfdp_entity.thaw(transaction_id)

# print("Transaction suspended")
# cfdp_entity.suspend(transaction_id)
# input("Press enter to resume")
# cfdp_entity.resume(transaction_id)

# print("Transaction suspended")
# cfdp_entity.suspend(transaction_id)
# input("Press enter to cancel")
# cfdp_entity.cancel(transaction_id)

# this should be a class method:
while cfdp_entity.machines:
    time.sleep(0.1)

input("Press <Enter> to finish.\n")
config.transport.disconnect()
cfdp_entity.shutdown()
