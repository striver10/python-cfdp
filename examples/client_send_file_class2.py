import logging

import cfdp
from cfdp.transport import ZmqTransport, UdpTransport
from cfdp.filestore import NativeFileStore

logging.basicConfig(level=logging.DEBUG)


#choice = int(input("Select transport (1=ZMQ, 2=UDP): "))
choice = 2
transport = ZmqTransport() if choice == 1 else UdpTransport()

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(
        1, "127.0.0.1:5551",
        transaction_inactivity_limit=20,
        positive_ack_timer_interval=5,
        positive_ack_timer_expiration_limit=2,
        nak_timer_interval=5,
        nak_timer_expiration_limit=2,
        )],
    filestore=NativeFileStore("./files/client"),
    transport=transport)

cfdp_entity = cfdp.CfdpEntity(config)
config.transport.connect(remote_entity_id=1)
config.transport.bind()

transaction_id = cfdp_entity.put(
    destination_id=1,
    #source_filename="/medium.txt",
    #destination_filename="/medium_remote.txt",
    transmission_mode=cfdp.TransmissionMode.ACKNOWLEDGED)

input("Press <Enter> to finish.\n")
config.transport.unbind()
config.transport.disconnect()
cfdp_entity.shutdown()
