import logging

import cfdp
from cfdp.transport import ZmqTransport, UdpTransport
from cfdp.filestore import NativeFileStore

logging.basicConfig(level=logging.INFO)


choice = int(input("Select transport (1=ZMQ, 2=UDP): "))
transport = ZmqTransport() if choice == 1 else UdpTransport()

config = cfdp.Config(
    local_entity=cfdp.LocalEntity(2, "127.0.0.1:5552"),
    remote_entities=[cfdp.RemoteEntity(1, "127.0.0.1:5551")],
    filestore=NativeFileStore("./files/client"),
    transport=transport)

cfdp_entity = cfdp.CfdpEntity(config)
config.transport.connect(remote_entity_id=1)
config.transport.bind()

cfdp_entity.put(
    destination_id=1,
    transmission_mode=cfdp.TransmissionMode.UNACKNOWLEDGED,
    messages_to_user=[
        cfdp.DirectoryListingRequest(
            remote_directory="/", local_file="/.listing.remote")])

input("Press <Enter> to finish.\n")
config.transport.unbind()
config.transport.disconnect()
cfdp_entity.shutdown()
